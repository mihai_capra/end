package capra.mihai.end.api

import capra.mihai.end.models.Products
import io.reactivex.Flowable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiServiceInterface {

    @GET("example.json")
    fun getAllProducts(): Flowable<Products>

    companion object Factory {
        fun create(): ApiServiceInterface {
            val okHttpInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            okHttpInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val okHttp = OkHttpClient.Builder()
            okHttp.addInterceptor(okHttpInterceptor)
            okHttp.retryOnConnectionFailure(true)

            val retrofit = Retrofit.Builder()
                .baseUrl("https://www.endclothing.com/media/catalog/")
                .client(okHttp.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(ApiServiceInterface::class.java)
        }
    }
}