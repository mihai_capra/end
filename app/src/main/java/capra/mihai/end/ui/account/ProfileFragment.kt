package capra.mihai.end.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import capra.mihai.end.R

class ProfileFragment : Fragment() {
    private lateinit var profileLayout: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        profileLayout = inflater.inflate(R.layout.profile_fragment, container, false)
        setHasOptionsMenu(true)
        return profileLayout
    }
}