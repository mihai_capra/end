package capra.mihai.end.ui.cart

import capra.mihai.end.ui.base.BaseContract

class CartContract {

    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun loadCartSuccess(message: String)
        fun loadCartFailure()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadCart()
    }
}