package capra.mihai.end.ui.products

import capra.mihai.end.models.Products
import capra.mihai.end.ui.base.BaseContract

class ProductsContract {

    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun loadProductsSuccess(products: Products)
        fun loadProductsError(error: Throwable)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadProducts()
    }

}