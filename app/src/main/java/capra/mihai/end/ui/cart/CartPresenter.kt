package capra.mihai.end.ui.cart

class CartPresenter : CartContract.Presenter {

    private lateinit var cartView: CartContract.View

    override fun loadCart() {}

    override fun subscribe() {}

    override fun unsubscribe() {}

    override fun attach(view: CartContract.View) {
        this.cartView = view
    }

}