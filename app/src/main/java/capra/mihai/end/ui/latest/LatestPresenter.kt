package capra.mihai.end.ui.latest

import capra.mihai.end.models.Products
import capra.mihai.end.models.SliderItem
import capra.mihai.end.api.ApiServiceInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LatestPresenter : LatestContract.Presenter {

    private lateinit var latestView: LatestContract.View
    private val productsDisposable = CompositeDisposable()
    private val api: ApiServiceInterface = ApiServiceInterface.create()

    override fun loadLatest() {
        latestView.showProgress(true)
        val subscribe = api.getAllProducts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ products ->
                latestView.showProgress(false)
                latestView.loadLatestSuccess(products)
            }, { error ->
                latestView.showProgress(false)
                error.printStackTrace()
                latestView.loadLatestError(error)
            })

        productsDisposable.add(subscribe)
    }

    override fun loadSliderData(): ArrayList<SliderItem> {
        val sliderItems = ArrayList<SliderItem>()

        sliderItems.add(
            SliderItem(
                "https://media.wonderlandmagazine.com/uploads/2018/11/SAINT-LAURENT_SPRING-SUMMER19_TRAVIS_HR_01.jpg",
                "SAINT\nLAURENT",
                "NEW SEASON COLLECTION."
            )
        )

        sliderItems.add(
            SliderItem(
                "https://img.hahabags.ru/201805/s-586951-y-3-off-white-casual-shoes-for-men.jpg",
                "OFF WHITE",
                "NEW SEASON SNEAKERS."
            )
        )

        sliderItems.add(
            SliderItem(
                "https://static1.squarespace.com/static/5332ea92e4b038baa34a56c8/t/569311ba40667a56fc91a33d/1452478911560/image.jpg",
                "JOHN ELLIOTT",
                "KEY PRICES FROM SS19."
            )
        )

        return sliderItems
    }


    override fun subscribe() {

    }

    override fun unsubscribe() {
        productsDisposable.clear()
    }

    override fun attach(view: LatestContract.View) {
        this.latestView = view
    }

}