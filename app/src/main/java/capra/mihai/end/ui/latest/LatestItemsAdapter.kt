package capra.mihai.end.ui.latest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import capra.mihai.end.R
import capra.mihai.end.models.Product
import kotlinx.android.synthetic.main.latest_item_layout.view.*

class LatestItemsAdapter(private val lastItems: List<Product>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return LatestItemsViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.latest_item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int = lastItems.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as LatestItemsViewHolder).bind(lastItems[position])
    }

    inner class LatestItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Product) = with(itemView) {
            itemView.itemImage.setImageURI(item.image)
            itemView.itemTitle.text = item.name
            itemView.itemPrice.text = "$ ${item.price}"
        }
    }

}