package capra.mihai.end.ui.latest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import capra.mihai.end.R
import capra.mihai.end.models.SliderItem
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.slider_item_layout.view.*

class LatestItemsSliderAdapter(private val sliderItems: List<SliderItem>) :
    SliderViewAdapter<SliderViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?): ViewHolder {
        return SliderItemViewHolder(
            LayoutInflater.from(parent!!.context)
                .inflate(R.layout.slider_item_layout, parent, false)
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, position: Int) {
        (viewHolder as SliderItemViewHolder).bind(sliderItems[position])
    }

    override fun getCount(): Int = sliderItems.size

    inner class SliderItemViewHolder(private val itemView: View) : SliderViewAdapter.ViewHolder(itemView) {
        fun bind(item: SliderItem) = with(itemView) {
            itemView.sliderImage.setImageURI(item.image)
            itemView.sliderTitle.text = item.title
            itemView.sliderSubTitle.text = item.subTitle
        }
    }
}