package capra.mihai.end.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import capra.mihai.end.R
import capra.mihai.end.di.component.DaggerFragmentComponent
import capra.mihai.end.di.module.FragmentModule
import kotlinx.android.synthetic.main.account_fragment.view.*
import kotlinx.android.synthetic.main.cart_fragment.view.*
import javax.inject.Inject

class AccountFragment : Fragment(), AccountContract.View {

    private lateinit var accountLayout: View

    @Inject
    lateinit var presenter: AccountContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        accountLayout = inflater.inflate(R.layout.account_fragment, container, false)
        setHasOptionsMenu(true)

        if (activity is AppCompatActivity) {
            accountLayout.accountToolbar.title = "PROFILE"
            (activity as AppCompatActivity).setSupportActionBar(accountLayout.cartToolbar)
        }

        accountLayout.accountViewPager.adapter =
            AccountPagerAdapter((activity as AppCompatActivity).supportFragmentManager)
        accountLayout.accountTabLayout.setupWithViewPager(accountLayout.accountViewPager)

        return accountLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.subscribe()
        presenter.attach(this)
    }

    private fun injectDependency() {
        val accountComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule()).build()
        accountComponent.inject(this)
    }

    companion object {
        val TAG: String = "AccountFragment"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }
}