package capra.mihai.end.ui.main

import capra.mihai.end.ui.base.BaseContract

class MainContract {

    interface View : BaseContract.View {
        fun showWishlistFragment()
        fun showMainFragment()
        fun showAccountFragment()
        fun showCartFragment()
    }

    interface Presenter : BaseContract.Presenter<View> {}

}