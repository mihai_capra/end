package capra.mihai.end.ui.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import capra.mihai.end.R
import capra.mihai.end.di.component.DaggerFragmentComponent
import capra.mihai.end.di.module.FragmentModule
import kotlinx.android.synthetic.main.wishlist_fragment.view.*
import javax.inject.Inject

class WishlistFragment : Fragment(), WishlistContract.View {

    private lateinit var wishlistLayout: View

    @Inject
    lateinit var presenter: WishlistContract.Presenter

    fun newInstance(): WishlistFragment {
        return WishlistFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        wishlistLayout = inflater.inflate(R.layout.wishlist_fragment, container, false)
        setHasOptionsMenu(true)

        if (activity is AppCompatActivity) {
            wishlistLayout.wishlistToolbar.title = "SAVED"
            (activity as AppCompatActivity).setSupportActionBar(wishlistLayout.wishlistToolbar)
        }

        return wishlistLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
        initView()
    }

    override fun showProgress(show: Boolean) {

    }

    override fun loadWishlistSuccess(message: String) {
        
    }

    private fun injectDependency() {
        val wishListComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .build()

        wishListComponent.inject(this)
    }

    private fun initView() {
        presenter.loadWishlist()
    }

    companion object {
        val TAG: String = "WishlistFragment"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }

}