package capra.mihai.end.ui.account

import capra.mihai.end.ui.base.BaseContract

class AccountContract {

    interface View : BaseContract.View {}

    interface Presenter : BaseContract.Presenter<View> {}
}