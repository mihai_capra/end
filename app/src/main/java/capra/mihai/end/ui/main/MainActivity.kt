package capra.mihai.end.ui.main

import android.opengl.Visibility
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import capra.mihai.end.R
import capra.mihai.end.di.component.DaggerActivityComponent
import capra.mihai.end.di.module.ActivityModule
import capra.mihai.end.ui.account.AccountFragment
import capra.mihai.end.ui.cart.CartFragment
import capra.mihai.end.ui.latest.LatestFragment
import capra.mihai.end.ui.wishlist.WishlistFragment
import capra.mihai.end.utils.FragmentsUtil
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_bar.*
import kotlinx.android.synthetic.main.bottom_bar.view.*
import kotlinx.android.synthetic.main.side_navigation.view.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), Animation.AnimationListener, MainContract.View {
    @Inject
    lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fadeIn = AnimationUtils.loadAnimation(this@MainActivity, R.anim.fade_in)
        splashLayout.visibility = View.VISIBLE
        fadeIn.setAnimationListener(this@MainActivity)
        splashLayout.startAnimation(fadeIn)
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        bottomBarSetup()

        drawerLayout.navigationDrawer.filter_close.setOnClickListener {
            drawerLayout.closeDrawer(Gravity.RIGHT)
        }

        injectDependency()
        presenter.attach(this)

    }

    private fun bottomBarSetup() {
        bottomBar.basketBtn.setOnClickListener {
            showCartFragment()
        }

        bottomBar.accountBtn.setOnClickListener {
            showAccountFragment()
        }

        bottomBar.wishListBtn.setOnClickListener {
            showWishlistFragment()
        }

        endHomeBtn.setOnClickListener {
            showMainFragment()
        }
    }

    override fun showWishlistFragment() {
        supportFragmentManager.popBackStackImmediate()
        FragmentsUtil.loadFragment(
            R.id.mainFrameLayout,
            WishlistFragment().newInstance(),
            WishlistFragment.TAG,
            supportFragmentManager,
            Pair(R.anim.fade_in, R.anim.fade_out)
        )
    }

    override fun showMainFragment() {
        FragmentsUtil.loadFragment(
            R.id.mainFrameLayout,
            LatestFragment(),
            LatestFragment.TAG,
            supportFragmentManager,
            Pair(R.anim.fade_in, R.anim.fade_out)
        )
    }

    override fun showAccountFragment() {
        supportFragmentManager.popBackStackImmediate()
        FragmentsUtil.loadFragmentWithBackStack(
            R.id.mainFrameLayout,
            AccountFragment(),
            AccountFragment.TAG,
            supportFragmentManager,
            Pair(R.anim.fade_in, R.anim.fade_out)
        )
    }

    override fun showCartFragment() {
        supportFragmentManager.popBackStackImmediate()
        FragmentsUtil.loadFragmentWithBackStack(
            R.id.mainFrameLayout,
            CartFragment(),
            CartFragment.TAG,
            supportFragmentManager,
            Pair(R.anim.fade_in, R.anim.fade_out)
        )
    }

    override fun onAnimationRepeat(p0: Animation?) {
        //no implementation needed
    }

    override fun onAnimationEnd(p0: Animation?) {
        splashLayout.visibility = View.GONE
        content_layout.visibility = View.VISIBLE
    }

    override fun onAnimationStart(p0: Animation?) {
        //no implementation needed
    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .build()

        activityComponent.inject(this)
    }

    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager
        val fragments = fragmentManager.fragments
        if (fragments.isNotEmpty()) {
            for (fragment in fragments) {
                if ((fragment is WishlistFragment && fragment.isVisible)
                    || (fragment is AccountFragment && fragment.isVisible)
                    || (fragment is CartFragment && fragment.isVisible)
                ) {
                    FragmentsUtil.loadFragment(
                        R.id.mainFrameLayout,
                        LatestFragment(),
                        LatestFragment.TAG,
                        supportFragmentManager,
                        Pair(R.anim.fade_in, R.anim.fade_out)
                    )
                }
                if (fragment is LatestFragment && fragment.isVisible) {
                    super.onBackPressed()
                }
            }
        } else {
            super.onBackPressed()
        }
    }
}