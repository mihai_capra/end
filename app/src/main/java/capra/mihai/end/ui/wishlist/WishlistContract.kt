package capra.mihai.end.ui.wishlist

import capra.mihai.end.ui.base.BaseContract

class WishlistContract {

    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun loadWishlistSuccess(message: String)
        // fun loadWishesFailure()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadWishlist()
    }

}