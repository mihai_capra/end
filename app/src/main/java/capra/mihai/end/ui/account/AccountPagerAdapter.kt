package capra.mihai.end.ui.account

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import capra.mihai.end.R
import capra.mihai.end.end.EndApplication
import capra.mihai.end.ui.account.ProfileFragment
import capra.mihai.end.ui.account.SettingsFragment

class AccountPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> {
                ProfileFragment()
            }
            1 -> {
                SettingsFragment()
            }
            else -> null
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> EndApplication.endInstance.getString(R.string.profile_title)
            1 -> EndApplication.endInstance.getString(R.string.settings_title)
            else -> super.getPageTitle(position)
        }
    }

}