package capra.mihai.end.ui.latest

import capra.mihai.end.models.Products
import capra.mihai.end.models.SliderItem
import capra.mihai.end.ui.base.BaseContract

class LatestContract {

    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun loadLatestSuccess(products: Products)
        fun loadLatestError(error: Throwable)
        fun showProductsFragment()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadLatest()
        fun loadSliderData(): ArrayList<SliderItem>
    }
}