package capra.mihai.end.ui.wishlist

class WishlistPresenter : WishlistContract.Presenter {

    private lateinit var view: WishlistContract.View

    override fun loadWishlist() {
        //network request
    }

    override fun subscribe() {
        //no implementation needed
    }

    override fun unsubscribe() {
        //no implementation needed
    }

    override fun attach(view: WishlistContract.View) {
        this.view = view
    }

}