package capra.mihai.end.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import capra.mihai.end.R

class SettingsFragment : Fragment() {
    private lateinit var settingsLayout: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        settingsLayout = inflater.inflate(R.layout.settings_fragment, container, false)
        setHasOptionsMenu(true)
        return settingsLayout
    }
}