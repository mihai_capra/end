package capra.mihai.end.ui.cart

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import capra.mihai.end.R
import capra.mihai.end.di.component.DaggerFragmentComponent
import capra.mihai.end.di.module.FragmentModule
import capra.mihai.end.ui.latest.LatestFragment
import capra.mihai.end.utils.FragmentsUtil
import kotlinx.android.synthetic.main.cart_fragment.view.*
import javax.inject.Inject

class CartFragment : Fragment(), CartContract.View {

    private lateinit var cartLayout: View

    @Inject
    lateinit var presenter: CartContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        cartLayout = inflater.inflate(R.layout.cart_fragment, container, false)
        setHasOptionsMenu(true)

        if (activity is AppCompatActivity) {
            cartLayout.cartToolbar.title = "CART"
            (activity as AppCompatActivity).setSupportActionBar(cartLayout.cartToolbar)
            if ((activity as AppCompatActivity).supportActionBar != null) {
                (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
                (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
        }

        return cartLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.subscribe()
        presenter.attach(this)
        initView()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.info_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.info_option -> {
                //info option clicked
            }
            android.R.id.home -> {
                FragmentsUtil.loadFragment(
                    R.id.mainFrameLayout,
                    LatestFragment(),
                    LatestFragment.TAG,
                    (activity as AppCompatActivity).supportFragmentManager,
                    Pair(R.anim.fade_in, R.anim.fade_out)
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun injectDependency() {
        val cartComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule()).build()
        cartComponent.inject(this)
    }

    override fun showProgress(show: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadCartSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadCartFailure() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun initView() {
        presenter.loadCart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }

    companion object {
        val TAG: String = "CartFragment"
    }

}