package capra.mihai.end.ui.products

import capra.mihai.end.api.ApiServiceInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ProductsPresenter : ProductsContract.Presenter {

    private lateinit var productsView: ProductsContract.View
    private val api: ApiServiceInterface = ApiServiceInterface.create()
    private val productsDisposable = CompositeDisposable()

    override fun loadProducts() {
        productsView.showProgress(true)
        val subscribe = api.getAllProducts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ products ->
                productsView.showProgress(false)
                productsView.loadProductsSuccess(products)
            }, { error ->
                error.printStackTrace()
                productsView.showProgress(false)
                productsView.loadProductsError(error)
            })

        productsDisposable.add(subscribe)
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
        productsDisposable.clear()
    }

    override fun attach(view: ProductsContract.View) {
        this.productsView = view
    }

}