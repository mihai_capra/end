package capra.mihai.end.ui.products

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import capra.mihai.end.R
import capra.mihai.end.models.Product
import kotlinx.android.synthetic.main.product_list_item_layout.view.*
import kotlinx.android.synthetic.main.simple_title_item_layout.view.*

class ProductsListAdapter(private val products: List<Any>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER -> HeaderViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.simple_title_item_layout, parent, false)
            )
            PRODUCTS -> ProductsViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.product_list_item_layout, parent, false)
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int = products.size

    fun isHeader(position: Int): Boolean = position == 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProductsViewHolder -> holder.bind(products[position] as Product)
            is HeaderViewHolder -> holder.bind(products[0] as String)
            else -> java.lang.IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (products[position]) {
            is String -> HEADER
            is Product -> PRODUCTS
            else -> throw IllegalArgumentException("Invalid type of data $position")
        }
    }

    inner class ProductsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(product: Product) = with(itemView) {
            itemView.productImage.setImageURI(product.image)
            itemView.productName.text = product.name
            itemView.productPrice.text = product.price
        }
    }

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(title: String) = with(itemView) {
            itemView.simpleLabel.text = title
        }
    }

    companion object {
        private const val HEADER = 0
        private const val PRODUCTS = 1
    }

}