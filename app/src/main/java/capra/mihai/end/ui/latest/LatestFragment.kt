package capra.mihai.end.ui.latest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import capra.mihai.end.R
import capra.mihai.end.di.component.DaggerFragmentComponent
import capra.mihai.end.di.module.FragmentModule
import capra.mihai.end.models.Products
import capra.mihai.end.ui.products.ProductsFragment
import capra.mihai.end.utils.FragmentsUtil
import capra.mihai.end.utils.ItemOffsetDecoration
import kotlinx.android.synthetic.main.end_fragment.*
import kotlinx.android.synthetic.main.end_fragment.view.*
import kotlinx.android.synthetic.main.products_fragment.view.*
import javax.inject.Inject

class LatestFragment : Fragment(), LatestContract.View {

    private lateinit var endLayout: View

    @Inject
    lateinit var presenter: LatestContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        endLayout = inflater.inflate(R.layout.end_fragment, container, false)

        endLayout.viewAllBtn.setOnClickListener {
            showProductsFragment()
        }

        return endLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
        presenter.subscribe()
        initView()
    }

    private fun injectDependency() {
        val latestComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .build()

        latestComponent.inject(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    override fun loadLatestSuccess(products: Products) {

        endLayout.endMainRecyclerView.apply {
            layoutManager = GridLayoutManager(
                (activity as AppCompatActivity),
                2,
                LinearLayoutManager.HORIZONTAL,
                false

            )
            adapter = LatestItemsAdapter(products.products.take(12))
        }

        endLayout.endMainRecyclerView.addItemDecoration(
            ItemOffsetDecoration(
                activity as AppCompatActivity,
                R.dimen.default_padding
            )
        )
    }

    override fun loadLatestError(error: Throwable) {
        error.printStackTrace()
    }

    override fun showProductsFragment() {
        FragmentsUtil.loadFragmentWithBackStack(
            R.id.mainFrameLayout,
            ProductsFragment(),
            ProductsFragment.TAG,
            (activity as AppCompatActivity).supportFragmentManager,
            Pair(R.anim.fade_in, R.anim.fade_out)
        )
    }

    private fun initView() {
        presenter.loadLatest()

        endLayout.endMainSlider.sliderIndicatorRadius = 4
        endLayout.endMainSlider.sliderIndicatorSelectedColor =
            ContextCompat.getColor(activity as AppCompatActivity, R.color.lightergrey)
        endLayout.endMainSlider.sliderIndicatorUnselectedColor =
            ContextCompat.getColor(activity as AppCompatActivity, R.color.white)
        endLayout.endMainSlider.sliderAdapter = LatestItemsSliderAdapter(presenter.loadSliderData())
    }

    companion object {
        val TAG: String = "LatestFragment"
    }

}