package capra.mihai.end.ui.account

class AccountPresenter : AccountContract.Presenter {

    private lateinit var accountView: AccountContract.View

    override fun subscribe() {}

    override fun unsubscribe() {}

    override fun attach(view: AccountContract.View) {
        this.accountView = view
    }

}