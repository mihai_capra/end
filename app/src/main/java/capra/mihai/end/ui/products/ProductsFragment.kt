package capra.mihai.end.ui.products

import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import capra.mihai.end.R
import capra.mihai.end.di.component.DaggerFragmentComponent
import capra.mihai.end.di.module.FragmentModule
import capra.mihai.end.models.Products
import capra.mihai.end.ui.latest.LatestFragment
import capra.mihai.end.utils.FragmentsUtil
import kotlinx.android.synthetic.main.end_fragment.*
import kotlinx.android.synthetic.main.products_fragment.view.*
import javax.inject.Inject

class ProductsFragment : Fragment(), ProductsContract.View, AdapterView.OnItemSelectedListener {

    private lateinit var productsLayout: View

    @Inject
    lateinit var presenter: ProductsContract.Presenter

    private var headerList: ArrayList<Any> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        productsLayout = inflater.inflate(R.layout.products_fragment, container, false)
        setHasOptionsMenu(true)

        if (activity is AppCompatActivity) {
            productsLayout.productsToolbar.title = "NEW THIS WEEK"
            (activity as AppCompatActivity).setSupportActionBar(productsLayout.productsToolbar)
            if ((activity as AppCompatActivity).supportActionBar != null) {
                (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
                (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
            (activity as AppCompatActivity).findViewById<DrawerLayout>(R.id.drawerLayout)
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }

        val sortValues = arrayOf(
            "Sort", "Price (High)", "Price (Low)"
        )
        productsLayout.sortSpinner.adapter =
            ArrayAdapter<String>(this@ProductsFragment.context!!, R.layout.simple_dropdown_spinner_item, sortValues)

        val viewValues = arrayOf(
            "Product", "Outfit", "Large", "Small"
        )
        productsLayout.viewSpinner.adapter =
            ArrayAdapter<String>(this@ProductsFragment.context!!, R.layout.simple_dropdown_spinner_item, viewValues)

        productsLayout.viewSpinner.onItemSelectedListener = this

        return productsLayout
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.subscribe()
        presenter.attach(this)
        initView()
    }

    private fun injectDependency() {
        val productsComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule()).build()

        productsComponent.inject(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.info_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.info_option -> {
                //info option clicked
            }
            android.R.id.home -> {
                FragmentsUtil.loadFragment(
                    R.id.mainFrameLayout,
                    LatestFragment(),
                    LatestFragment.TAG,
                    (activity as AppCompatActivity).supportFragmentManager,
                    Pair(R.anim.fade_in, R.anim.fade_out)
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    override fun loadProductsSuccess(products: Products) {
        headerList = ArrayList(products.products)
        headerList.add(0, "${products.product_count} items")

        setupGridRecyclerView(headerList)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (position) {
            0 -> if (headerList.isNotEmpty()) setupGridRecyclerView(headerList)
            2 -> if (headerList.isNotEmpty()) setupLinearRecyclerView(headerList)
            3 -> if (headerList.isNotEmpty()) setupGridRecyclerView(headerList)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun setupGridRecyclerView(customList: ArrayList<Any>) {
        val adapterProducts = ProductsListAdapter(customList)

        val manager = GridLayoutManager(
            (activity as AppCompatActivity),
            2,
            RecyclerView.VERTICAL,
            false
        )

        manager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (adapterProducts.isHeader(position)) {
                    manager.spanCount
                } else {
                    1
                }
            }
        }

        productsLayout.productsRecycler.apply {
            layoutManager = manager
            adapter = adapterProducts
        }
    }

    private fun setupLinearRecyclerView(customList: ArrayList<Any>) {
        val adapterProducts = ProductsListAdapter(customList)

        val manager = LinearLayoutManager(
            (activity as AppCompatActivity),
            RecyclerView.VERTICAL,
            false
        )

        productsLayout.productsRecycler.apply {
            layoutManager = manager
            adapter = adapterProducts
        }
    }

    override fun loadProductsError(error: Throwable) {
        headerList = ArrayList()
        error.printStackTrace()
    }

    private fun initView() {
        presenter.loadProducts()
        productsLayout.filter.setOnClickListener {
            (activity as AppCompatActivity).findViewById<DrawerLayout>(R.id.drawerLayout)
                .openDrawer(Gravity.RIGHT)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
        (activity as AppCompatActivity).findViewById<DrawerLayout>(R.id.drawerLayout)
            .setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    companion object {
        val TAG: String = "ProductsFragment"
    }
}