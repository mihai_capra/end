package capra.mihai.end.end

import android.app.Application
import capra.mihai.end.di.component.ApplicationComponent
import capra.mihai.end.di.component.DaggerApplicationComponent
import capra.mihai.end.di.module.ApplicationModule
import com.facebook.drawee.backends.pipeline.Fresco

class EndApplication : Application() {
    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        endInstance = this
        Fresco.initialize(this)
        setup()
    }

    fun setup() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    fun getApplicationComponent(): ApplicationComponent = component

    companion object {
        lateinit var endInstance: EndApplication
            private set
    }

}