package capra.mihai.end.di.component

import capra.mihai.end.di.module.FragmentModule
import capra.mihai.end.ui.account.AccountFragment
import capra.mihai.end.ui.cart.CartFragment
import capra.mihai.end.ui.latest.LatestFragment
import capra.mihai.end.ui.products.ProductsFragment
import capra.mihai.end.ui.wishlist.WishlistFragment
import dagger.Component

@Component(modules = [FragmentModule::class])
interface FragmentComponent {

    fun inject(wishlistFragment: WishlistFragment)

    fun inject(latestFragment: LatestFragment)

    fun inject(productsFragment: ProductsFragment)

    fun inject(accountFragment: AccountFragment)

    fun inject(cartFragment: CartFragment)
}