package capra.mihai.end.di.module

import android.app.Application
import capra.mihai.end.di.scope.PerApplication
import capra.mihai.end.end.EndApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val endApp: EndApplication) {

    @Singleton
    @Provides
    @PerApplication
    fun provideApplication(): Application {
        return endApp
    }
}