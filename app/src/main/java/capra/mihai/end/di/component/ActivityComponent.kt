package capra.mihai.end.di.component

import capra.mihai.end.di.module.ActivityModule
import capra.mihai.end.ui.main.MainActivity
import dagger.Component

@Component(modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

}