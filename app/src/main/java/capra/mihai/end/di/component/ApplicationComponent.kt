package capra.mihai.end.di.component

import capra.mihai.end.di.module.ApplicationModule
import capra.mihai.end.end.EndApplication
import dagger.Component

@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun inject(application: EndApplication)
}