package capra.mihai.end.di.module

import android.app.Activity
import capra.mihai.end.ui.main.MainContract
import capra.mihai.end.ui.main.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun providePresenter(): MainContract.Presenter {
        return MainPresenter()
    }

}