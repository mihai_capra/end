package capra.mihai.end.di.module

import capra.mihai.end.ui.account.AccountContract
import capra.mihai.end.ui.account.AccountPresenter
import capra.mihai.end.ui.cart.CartContract
import capra.mihai.end.ui.cart.CartFragment
import capra.mihai.end.ui.cart.CartPresenter
import capra.mihai.end.ui.latest.LatestContract
import capra.mihai.end.ui.latest.LatestPresenter
import capra.mihai.end.ui.products.ProductsContract
import capra.mihai.end.ui.products.ProductsPresenter
import capra.mihai.end.ui.wishlist.WishlistContract
import capra.mihai.end.ui.wishlist.WishlistPresenter
import dagger.Module
import dagger.Provides

@Module
class FragmentModule {

    @Provides
    fun provideWishlistPresenter(): WishlistContract.Presenter {
        return WishlistPresenter()
    }

    @Provides
    fun provideLatestPresenter(): LatestContract.Presenter {
        return LatestPresenter()
    }

    @Provides
    fun provideProductsPresenter(): ProductsContract.Presenter {
        return ProductsPresenter()
    }

    @Provides
    fun provideAccountPresenter(): AccountContract.Presenter {
        return AccountPresenter()
    }

    @Provides
    fun provideCartProvideer(): CartContract.Presenter {
        return CartPresenter()
    }

}