package capra.mihai.end.models

data class SliderItem(
    val image: String,
    val title: String,
    val subTitle: String
)