package capra.mihai.end.models

data class Products(
    val product_count: Int,
    val products: List<Product>,
    val title: String
)

data class Product(
    val id: String,
    val image: String,
    val name: String,
    val price: String
)