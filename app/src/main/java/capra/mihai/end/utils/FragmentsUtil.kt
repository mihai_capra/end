package capra.mihai.end.utils

import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import capra.mihai.end.R
import org.jetbrains.annotations.NotNull

object FragmentsUtil {

    fun loadFragmentWithBackStack(
        @NonNull containerId: Int,
        @NotNull fragment: Fragment,
        @NonNull tag: String,
        @NonNull paramFragmentManager: FragmentManager,
        @NonNull animations: Pair<Int, Int>
    ) {
        paramFragmentManager
            .beginTransaction()
            .setCustomAnimations(animations.first, animations.second)
            .replace(containerId, fragment, tag)
            .addToBackStack(null)
            .commit()
    }

    fun loadFragment(
        @NonNull containerId: Int,
        @NonNull fragment: Fragment,
        @NonNull tag: String,
        @NonNull paramFragmentManager: FragmentManager,
        @NonNull animations: Pair<Int, Int>
    ) {
        if (paramFragmentManager.backStackEntryCount > 0) {
            paramFragmentManager.popBackStackImmediate()
        }

        paramFragmentManager
            .beginTransaction()
            .setCustomAnimations(animations.first, animations.second)
            .replace(containerId, fragment, tag)
            .commit()
    }

}