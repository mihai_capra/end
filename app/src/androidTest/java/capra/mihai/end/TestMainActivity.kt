package capra.mihai.end

import androidx.recyclerview.widget.RecyclerView
import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import capra.mihai.end.ui.latest.LatestItemsAdapter
import capra.mihai.end.ui.main.MainActivity
import capra.mihai.end.ui.products.ProductsListAdapter
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TestMainActivity {
    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        Assert.assertEquals("capra.mihai.end", appContext.packageName)
    }

    @Test
    fun checkBottomNavigationBar() {
        onView(withId(R.id.bottomBar)).check(matches(isDisplayed()))
        onView(withId(R.id.menuSearchBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.wishListBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.endHomeBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.accountBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.basketBtn)).check(matches(isDisplayed()))
    }

    @Test
    fun checkBottomBarOptionsClick() {
        onView(withId(R.id.menuSearchBtn)).perform(click())
        onView(withId(R.id.wishListBtn)).perform(click())
        onView(withId(R.id.endHomeBtn)).perform(click())
        onView(withId(R.id.accountBtn)).perform(click())
        onView(withId(R.id.basketBtn)).perform(click())
    }

    @Test
    fun checkWishListFragment() {
        onView(withId(R.id.wishListBtn)).perform(click())
        onView(withId(R.id.wishListFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.wishListLoginBtn)).perform(click())
    }

    @Test
    fun checkEndMainFragment() {
        onView(withId(R.id.endHomeBtn)).perform(click())
        onView(withId(R.id.endMainFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.endMainSlider)).check(matches(isDisplayed()))
        onView(withId(R.id.endMainRecyclerView)).check(matches(isDisplayed()))
        onView(withId(R.id.endMainRecyclerView)).perform(
            RecyclerViewActions.scrollToPosition<LatestItemsAdapter.LatestItemsViewHolder>(
                10
            )
        )
        onView(withId(R.id.endMainRecyclerView)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                0
            )
        )
        onView(withId(R.id.endMainRecyclerView))
            .perform(RecyclerViewActions.actionOnItemAtPosition<LatestItemsAdapter.LatestItemsViewHolder>(0, click()))

        onView(withId(R.id.viewAllBtn)).perform(click())
    }

    @Test
    fun checkProductsFragment() {
        onView(withId(R.id.endHomeBtn)).perform(click())
        onView(withId(R.id.endMainFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.viewAllBtn)).perform(click())
        onView(withId(R.id.productsFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.productsRecycler)).check(matches(isDisplayed()))
        onView(withId(R.id.productsRecycler)).perform(
            RecyclerViewActions.scrollToPosition<ProductsListAdapter.ProductsViewHolder>(
                24
            )
        )
        onView(withId(R.id.productsRecycler)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                0
            )
        )
        onView(withId(R.id.productsRecycler))
            .perform(RecyclerViewActions.actionOnItemAtPosition<ProductsListAdapter.ProductsViewHolder>(12, click()))

        //TODO: add toolbar tests
    }

    @Test
    fun checkAccountFragment() {
        onView(withId(R.id.accountBtn)).perform(click())
        onView(withId(R.id.accountFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.profileFragment)).check(matches(isDisplayed()))
        onView(withId(R.id.profileLoginBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.profileLoginBtn)).perform(click())
    }

    @Test
    fun checkCartFragment() {
        onView(withId(R.id.basketBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.basketBtn)).perform(click())
    }
}